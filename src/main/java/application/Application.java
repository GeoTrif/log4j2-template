package application;

import service.BasicServiceClass;

public class Application {

    private static final BasicServiceClass BASIC_SERVICE_CLASS = new BasicServiceClass();

    public static void main(String[] args) {
        BASIC_SERVICE_CLASS.logInfo();
    }
}
