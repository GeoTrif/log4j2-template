package service;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BasicServiceClass {

    private static final Logger LOGGER = LogManager.getLogger(BasicServiceClass.class);

    private static final String LOG_TESTING_MESSAGE = "Log testing.";
    private static final String INFO_TESTING_MESSAGE = "Info testing.";
    private static final String WARN_TESTING_MESSAGE = "Warn testing.";
    private static final String ERROR_TESTING_MESSAGE = "Error testing.";
    private static final String DEBUG_TESTING_MESSAGE = "Debug testing.";
    private static final String FATAL_TESTING_MESSAGE = "Fatal testing.";
    private static final String TRACE_TESTING_MESSAGE = "Trace testing.";

    public void logInfo() {
        LOGGER.info(INFO_TESTING_MESSAGE);
        LOGGER.warn(WARN_TESTING_MESSAGE);
        LOGGER.error(ERROR_TESTING_MESSAGE);
        LOGGER.debug(DEBUG_TESTING_MESSAGE);
        LOGGER.fatal(FATAL_TESTING_MESSAGE);
        LOGGER.log(Level.INFO, LOG_TESTING_MESSAGE);
        LOGGER.trace(TRACE_TESTING_MESSAGE);
    }
}
